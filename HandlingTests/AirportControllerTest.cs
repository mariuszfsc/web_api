﻿using HandlingAPI.Controllers;
using HandlingDatabase.DataBase;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace HandlingTests
{
    class AirportControllerTest
    {
        static Airports addAirport = new Airports { Name = "CityOfLight", IacoCode = "COLX", CountryId = 999, ActualCode = "TST" };
        static Airports editAirport = new Airports { Id = 212, Name = "CityOfTest", IacoCode = "COTX", CountryId = 777, ActualCode = "MMM" };

        static Mock<ILogger<AirportsController>> mock;
        static ILogger<AirportsController> logger;
        static AirportsController controller;

        [SetUp]
        public void SetUp()
        {
            mock = new Mock<ILogger<AirportsController>>();
            logger = mock.Object;
            //logger = Mock.Of<ILogger<AirportsController>>();
            controller = new AirportsController(logger);

        }

        [Test]
        public void GetAllAirportsTest()
        {
            var response = controller.Get();
            var okResponse = response as OkObjectResult;

            Assert.IsNotNull(okResponse);
            Assert.AreEqual(200, okResponse.StatusCode);
        }

        [Test]
        public void GetAirportByIdTest()
        {
            var response = controller.Get(1);
            var okResponse = response as OkObjectResult;

            Assert.IsNotNull(okResponse);
            Assert.AreEqual(200, okResponse.StatusCode);
        }

        [Test]
        public void GetAirportByIdTestFail()
        {
            var response = controller.Get(222);
            var notFoundResponse = response as NotFoundResult;

            Assert.IsNotNull(notFoundResponse);
            Assert.AreEqual(404, notFoundResponse.StatusCode);
        }

        [Test]
        public void AddAirportTest()
        {
            var response = controller.Edit(addAirport);
            var okResponse = response as OkResult;
            Assert.IsNotNull(okResponse);
            Assert.AreEqual(200, okResponse.StatusCode);
        }

        //[Test]  //Test zwraca zawsze ok
        //public void AddAirportTestFail()
        //{
        //    var response = controller.Edit(addAirport);
        //    var notFoundResponse = response as NotFoundResult;

        //    Assert.IsNotNull(notFoundResponse);
        //    Assert.AreEqual(404, notFoundResponse.StatusCode);
        //}

        [Test]
        public void EditAirportTest()
        {
            var response = controller.Edit(editAirport);
            var okResponse = response as OkObjectResult;

            Assert.IsNotNull(okResponse);
            Assert.AreEqual(200, okResponse.StatusCode);
        }


        [Test]
        public void EditAirportTestFail()
        {
            var response = controller.Edit(editAirport);
            var notFoundResponse = response as NotFoundResult;

            Assert.IsNotNull(notFoundResponse);
            Assert.AreEqual(404, notFoundResponse.StatusCode);
        }

        [Test]
        public void DeleteAirportTest()
        {
            var response = controller.Delete(212);
            var okResponse = response as OkObjectResult;

            Assert.IsNotNull(okResponse);
            Assert.AreEqual(200, okResponse.StatusCode);
        }

        [Test]
        public void DeleteAirportTestFail()
        {
            var response = controller.Delete(333);
            var notFoundResponse = response as NotFoundResult;

            Assert.IsNotNull(notFoundResponse);
            Assert.AreEqual(404, notFoundResponse.StatusCode);
        }
    }
}
