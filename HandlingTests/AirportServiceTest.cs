using HandlingDatabase.DataBase;
using HandlingDatabase.Services;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Configuration;
using System.Linq;

namespace HandlingTests
{

    public class AirportServiceTest
    {
        static Airports airportAMS = new Airports { Id = 1, Name = "AMSTERDAM", IacoCode = "EHAM", CountryId = 156, ActualCode = "AMS" };
        static Airports testAirport = new Airports { Id = 211, Name = "TESTER", IacoCode = "TEST", CountryId = 999, ActualCode = "XXX" };
        static AirportService service;
        static int num; 
        
        [SetUp]
        public void Setup()
        {
            service = new AirportService();
            num = service.GetAirports().Count();
        }

        [Test]
        public void GetAllAirportsServiceTest()
        {
            var response = service.GetAirports();

            Assert.Equals(210, response.Count());
        }

        [Test]
        [TestCase(1)]
        public void GetAirportServiceByIdTest(int id)
        {
            var response = service.GetAirport(id);

            Assert.AreEqual(airportAMS, response);
        }

        [Test]
        [TestCase(211)]
        public void GetNotExistingAirportServiceByIdTest(int id)
        {
            var response = service.GetAirport(id);

            Assert.IsNull(response);
        }

        [Test]
        [TestCaseSource("testAirport")]
        public void AddAirportServiceTest(Airports airport)
        {
            service.AddAirport(airport);
            var response = service.GetAirport(num);

            Assert.AreEqual(airport, response);
        }

        [Test]
        [TestCaseSource("testAirport")]
        public void EditAirportServiceTest(Airports airport)
        {
            airport.Name = "TESTCITY";
            service.EditAirport(airport);

            Assert.AreEqual("TESTCITY", service.GetAirport(num));
        }

        [Test]
        public void AddNotExistingAirportServiceTest(Airports airport)
        {
            airport = new Airports { Id = 255, Name = "TESTCITY", CountryId = null };
            service.AddAirport(airport);
            var response = service.GetAirport(num);

            Assert.AreNotEqual(airport.Name, response.Name);
        }

        [Test]
        [TestCase(211)]
        public void DeleteAirportServiceByIdTest(int id)
        {
            service.DeleteAirport(id);
            var response = service.GetAirport(id);

            Assert.AreNotEqual(num, id);
        }

        [Test]
        [TestCase(211)]
        public void DeleteNotExistingAirportServiceByIdTest(int id)
        {
            service.DeleteAirport(id);
            Assert.Fail();
        }
    }
}