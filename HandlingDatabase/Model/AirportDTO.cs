﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandlingDatabase.Model
{
    public class AirportDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IATACode { get; set; }
        public int? CountryId { get; set; }
        public string Country { get; set; }
        public string ICAOCode { get; set; }
        public DateTime? LastActivity { get; set; }
        public int? RegionId { get; set; }
        public string RegionName { get; set; }
        public int? NoOfServices { get; set; }
    }
}
