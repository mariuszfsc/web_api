﻿using HandlingDatabase.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandlingDatabase.Services
{
    public class CountryServices
    {
        public IEnumerable<Country> GetCountrys()
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.Country.ToList();
            }
        }
        public void AddCountry(Country country)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                context.Country.Add(country);
                context.SaveChanges();
            }
        }

        public void EditCountry(Country country)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                context.Country.AddOrUpdate(country);
                context.SaveChanges();
            }
        }

        public void DeleteCountry(int id)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                var country = context.Country.Where(x => x.Id == id).FirstOrDefault();
                context.Country.Remove(country);
            }
        }

        public Country GetCountry(int id)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.Country.FirstOrDefault(x => x.Id == id);
            }
        }
    }
}
