﻿using HandlingDatabase.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandlingDatabase.Services
{
    public class AtributeServices
    {
        public IEnumerable<DataBase.Attributes> GetAttributes()
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.Attributes.ToList();
            }
        }
        public void AddAttributes(DataBase.Attributes atr)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                context.Attributes.Add(atr);
                context.SaveChanges();
            }
        }

        public void EditAttributes(DataBase.Attributes atr)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                context.Attributes.AddOrUpdate(atr);
                context.SaveChanges();
            }
        }

        public void DeleteAttributes(int id)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                var atr = context.Attributes.Where(x => x.Id == id).FirstOrDefault();
                context.Attributes.Remove(atr);
                context.SaveChanges();
            }
        }

        public DataBase.Attributes GetAttributes(int id)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.Attributes.FirstOrDefault(x => x.Id == id);
            }
        }
    }
}
