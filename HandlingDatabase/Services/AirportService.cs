﻿using HandlingDatabase.DataBase;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.Migrations;
using HandlingDatabase.Model;

namespace HandlingDatabase.Services
{
    public class AirportService
    {
        public IEnumerable<AirportDTO> GetAirports()
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                var res = from e1 in context.Airports
                          join e2 in context.Country on e1.CountryId equals e2.Id
                          join e3 in context.Regions on e1.RegionId equals e3.Id
                          select new AirportDTO
                          {
                              Id = e1.Id,
                              Name = e1.Name,
                              Country = e2.Name,
                              IATACode= e1.IATACode,
                              ICAOCode = e1.ICAOCode,
                              LastActivity = e1.LastActivity,
                              NoOfServices = e1.NoOfServices,
                              RegionName = e3.RegionName
                          };

                return res.ToList();
            }
        }
        public void AddAirport(Airports airports)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                context.Airports.Add(airports);
                context.SaveChanges();
            }
        }

        public void EditAirport(Airports airports)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                context.Airports.AddOrUpdate(airports);
                context.SaveChanges();
            }
        }

        public void DeleteAirport(int id)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                var airport = context.Airports.Where(x => x.Id == id).FirstOrDefault();
                context.Airports.Remove(airport);
                context.SaveChanges();
            }
        }

        public Airports GetAirport(int id)
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.Airports.FirstOrDefault(x => x.Id == id);
            }
        }
    }
}
