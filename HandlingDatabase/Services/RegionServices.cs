﻿using HandlingDatabase.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandlingDatabase.Services
{
    public class RegionServices
    {
        public IEnumerable<Regions> GetRegions()
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.Regions.ToList();
            }
        }
    }
}
