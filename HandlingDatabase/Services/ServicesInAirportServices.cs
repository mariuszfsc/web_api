﻿using HandlingDatabase.DataBase;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HandlingDatabase.Services
{
    public class ServicesInAirportServices
    {
        public IEnumerable<ServicesInPort> GetServices()
        {
            using (HandlingEntitiesDatabase context = new HandlingEntitiesDatabase())
            {
                return context.ServicesInPort.ToList();
            }
        }
    }
}
