//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HandlingDatabase.DataBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Airports
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IATACode { get; set; }
        public Nullable<int> CountryId { get; set; }
        public string ICAOCode { get; set; }
        public Nullable<System.DateTime> LastActivity { get; set; }
        public Nullable<int> RegionId { get; set; }
        public Nullable<int> NoOfServices { get; set; }
    }
}
