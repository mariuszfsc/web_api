using HandlingAPI.Controllers;
using HandlingDatabase.DataBase;
using HandlingDatabase.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Xunit.Sdk;

namespace HandlingUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        static Airports addAirport = new Airports { Id = 6666, Name = "CityOfLight", IacoCode = "COLX", CountryId = 999, ActualCode = "TST" };
        static Airports editAirport = new Airports { Id = 212, Name = "CityOfTest", IacoCode = "COTX", CountryId = 777, ActualCode = "MMM" };

        static Mock<ILogger<AirportsController>> mock;
        static ILogger<AirportsController> logger;
        static AirportsController controller;

        public UnitTest1()
        {
            mock = new Mock<ILogger<AirportsController>>();
            logger = mock.Object;
            AirportService airportServices = new AirportService();
            //logger = Mock.Of<ILogger<AirportsController>>();
            controller = new AirportsController(logger);
        }

        [TestMethod]
        public void TestMethod1()
        {
            var response = controller.Get();
            var okResponse = response as OkObjectResult;

            Assert.IsNotNull(okResponse);
            Assert.AreEqual(200, okResponse.StatusCode);
        }
    }
}
