﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HandlingAPI.Models
{
    public class Airports
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IacoCode { get; set; }
        public Nullable<int> CountryId { get; set; }
        public string ActualCode { get; set; }
    }
}
