﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HandlingDatabase.DataBase;
using HandlingDatabase.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HandlingAPI.Controllers
{
    [Route("api/countries")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        CountryServices countryServices = new CountryServices();

        private readonly ILogger<CountriesController> _logger;

        public CountriesController(ILogger<CountriesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Country> Get()
        {
            return countryServices.GetCountrys();
        }

        [HttpGet("{id}")]
        public Country Get(int id)
        {
            return countryServices.GetCountry(id);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            countryServices.DeleteCountry(id);
        }

        [HttpPost]
        public void Edit(Country airport)
        {
            countryServices.EditCountry(airport);
        }
    }
}