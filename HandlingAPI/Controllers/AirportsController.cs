﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HandlingDatabase.DataBase;
using HandlingDatabase.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HandlingAPI.Controllers
{
    [ApiController]
    [Route("api/airports")]
    public class AirportsController : ControllerBase
    {
        AirportService airportServices = new AirportService();

        private readonly ILogger<AirportsController> _logger;

        public AirportsController(ILogger<AirportsController> logger)
        {
            _logger = logger;
            airportServices = new AirportService();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get()
        {
            var airports =  airportServices.GetAirports();
            if (airports.Count() > 0)
            {
                return Ok(airports);
            }
            else
                return NotFound();
           
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get(int id)
        {
            var airport = airportServices.GetAirport(id);
            if (airport != null)
            {
                return Ok(airport);
            }
            else
                return NotFound();
        }
        
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Delete(int id)
        {
            try
            {
                airportServices.DeleteAirport(id);
                return Ok();
            }
            catch
            {
                return NoContent();
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult Edit(Airports airport)
        {
            try
            {
                airportServices.EditAirport(airport);
                return Ok();
            }
            catch
            {
                return NoContent();
            }
        }

    }
}
