﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security;
using System.DirectoryServices.AccountManagement;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using RouteAttribute = Microsoft.AspNetCore.Components.RouteAttribute;
using Microsoft.AspNetCore.Cors;

namespace HandlingAPI.Controllers
{
    [ApiController]
    //[EnableCors("AllowMyOrgin")]
    [Route("api/permissionUser/")]
    public class PermissionUserController : ControllerBase
    {
        [HttpGet("{user}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public IActionResult GetGroups(string user)
        {
            if (user != null)
            {
                List<GroupPrincipal> result = new List<GroupPrincipal>();

                PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);

                UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(yourDomain, user);

                if (userPrincipal != null)
                {
                    PrincipalSearchResult<Principal> groups = userPrincipal.GetAuthorizationGroups();

                    foreach (Principal p in groups)
                    {
                        if (p is GroupPrincipal)
                        {
                            result.Add((GroupPrincipal)p);
                        }
                    }
                }

                return Ok(result);
            }
            return NotFound();
        }

        [HttpGet("{user}/{password}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        //[EnableCors("AllowMyOrgin")]
        public IActionResult CheckUser(string user, string password)
        {
            bool isValid = false;
            try
            {

                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, "AX"))
                {
                    // validate the credentials
                    isValid = pc.ValidateCredentials(user, password, ContextOptions.Negotiate);

                    using (UserPrincipal us = UserPrincipal.FindByIdentity(pc, user))
                    {
                        string test = us.Description;

                        GetGroups(user);
                    }
                }
                if (isValid)
                {
                    return Ok(isValid);
                }
                else
                    return NoContent();
            }
            catch
            {
                return NotFound(isValid);
            }
        }
    }
}