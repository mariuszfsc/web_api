﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HandlingDatabase.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HandlingAPI.Controllers
{
    [Route("api/regions")]
    [ApiController]
    public class RegionsController : ControllerBase
    {
        RegionServices regionsServices = new RegionServices();

        private readonly ILogger<RegionsController> _logger;

        public RegionsController(ILogger<RegionsController> logger)
        {
            _logger = logger;
            regionsServices = new RegionServices();
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get()
        {
            var regions = regionsServices.GetRegions();
            if (regions.Count() > 0)
            {
                return Ok(regions);
            }
            else
                return NotFound();

        }
    }
}