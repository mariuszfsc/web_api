﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HandlingDatabase.DataBase;
using HandlingDatabase.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HandlingAPI.Controllers
{
    [Route("api/attributes")]
    [ApiController]
    public class AttributesController : ControllerBase
    {
        AtributeServices attributesServices = new AtributeServices();

        private readonly ILogger<AttributesController> _logger;

        public AttributesController(ILogger<AttributesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Attributes> Get()
        {
            return attributesServices.GetAttributes();
        }

        [HttpGet("{id}")]
        public Attributes Get(int id)
        {
            return attributesServices.GetAttributes(id);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            attributesServices.DeleteAttributes(id);
        }

        [HttpPost]
        public void Edit(Attributes attributes)
        {
            attributesServices.EditAttributes(attributes);
        }
    }
}