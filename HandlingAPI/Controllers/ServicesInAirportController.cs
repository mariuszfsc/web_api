﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HandlingDatabase.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HandlingAPI.Controllers
{
    [Route("api/services")]
    [ApiController]
    public class ServicesInAirportController : ControllerBase
    {
        ServicesInAirportServices servicesInAirportServices = new ServicesInAirportServices();

        private readonly ILogger<ServicesInAirportController> _logger;

        public ServicesInAirportController(ILogger<ServicesInAirportController> logger)
        {
            _logger = logger;
            servicesInAirportServices = new ServicesInAirportServices();
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult Get()
        {
            var services = servicesInAirportServices.GetServices();
            if (services.Count() > 0)
            {
                return Ok(services);
            }
            else
                return NotFound();

        }
    }
}