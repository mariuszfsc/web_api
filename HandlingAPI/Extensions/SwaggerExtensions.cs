﻿namespace HandlingAPI.Extensions
{
    using System.Linq;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.Extensions.DependencyInjection;
    using Swashbuckle.AspNetCore.Swagger;
    using Swashbuckle.AspNetCore.SwaggerUI;
    //using Swashbuckle.Swagger.Model;

    public static class SwaggerExtensions
    {
        private const string DocumentationTitle = "LOT Handling API";
        private const string ApiVersionOne = "v1";
        private const string ApiVersionOneJsonEndpoint = "v1/swagger.json";
        private const string ApiVersionOneName = "LOT Handling API v1";

        public static void AddSwaggerDocumentation(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(ApiVersionOne, new Microsoft.OpenApi.Models.OpenApiInfo { Title = DocumentationTitle, Version = ApiVersionOne });
                options.EnableAnnotations();
                options.CustomSchemaIds(x => x.FullName);
 

                options.ResolveConflictingActions(x =>
                {
                    var apiDescription = x.First();
                    var apiParameterDescriptions = x.SelectMany(y => y.ParameterDescriptions).ToList();
                    apiDescription.ParameterDescriptions.Clear();
                    foreach (var apiParameterDescription in apiParameterDescriptions)
                    {
                        apiDescription.ParameterDescriptions.Add(apiParameterDescription);
                    }

                    return apiDescription;
                });
            });
        }

        public static void UseSwaggerDocumentation(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseSwagger(null);
            applicationBuilder.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint(ApiVersionOneJsonEndpoint, ApiVersionOneName);
                options.DocExpansion(DocExpansion.None);
            });
        }
    }
}
